# scroll-phat-hd-timer

### *A timer for the raspberry pi using a [Scroll pHAT HD](https://shop.pimoroni.com/products/scroll-phat-hd) controllable from the web*

## Video 

[![Demo video](https://img.youtube.com/vi/PUpbV0AQUYQ/0.jpg)](https://youtu.be/PUpbV0AQUYQ)

The reason I made it is to use it with the [PomoDoneApp](https://pomodoneapp.com/) task timer on my computer which uses the [Pomodoro Technique](https://en.wikipedia.org/wiki/Pomodoro_Technique). With PomoDoneApp, you can configure triggers for what happens when timer starts, stops, etc. I thought I could hook up the Raspberry Pi Zero with it.

## Features
- Start/stop 25 min timer 
- Pause/resume current timer
- Start 5 min timer (break)

## Installation

Requires: `python-flask` and [`scrollphathd`](https://github.com/pimoroni/scroll-phat-hd)

```
curl https://get.pimoroni.com/scrollphathd | bash
pip install python-flask
git clone https://bitbucket.org/zboarda/scroll-phat-hd-timer.git
```

## Usage
Access by finding the IP address of the raspberry pi or by using the `hostname` if on the local network (normally `raspberrypi.local`).
`POST` the timer using `curl`.

### Linux/macOS
`curl -X POST <pi address>:8080/scrollphat/<command>`
### Windows Powershell
`curl -Method POST -Uri <pi address>:8080/scrollphat/<command>`

## Commands
- Start `start`
- Stop `stop`
- Pause `pause`
- Resume `resume`
- Start 5 min timer (break timer) `start-break`
- Stop 5 min timer (break timer) `stop-break`

## Example
_Start timer (Windows)_

`curl -Method POST -Uri <pi address>:8080/scrollphathd/start`

_Stop timer (Windows)_

`curl -Method POST -Uri <pi address>:8080/scrollphathd/stop`

## Thanks
Thanks to [PomoDoneApp](https://pomodoneapp.com/) team for an amazing task timer and the custom integration to make this possible.

Thanks to the [Pimoroni team](https://github.com/pimoroni) for the [Scroll pHAT HD](https://shop.pimoroni.com/products/scroll-phat-hd), the [http server](https://github.com/pimoroni/scroll-phat-hd/tree/master/library/scrollphathd/api), and the [clock example](https://github.com/pimoroni/scroll-phat-hd/blob/master/examples/clock.py).

## Licence

```
MIT Licence

Original work (c) 2017 Pimoroni Ltd.
Modified work (c) 2018 Timothy Kist.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```