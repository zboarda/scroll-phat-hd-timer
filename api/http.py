import scrollphathd

import threading
import time
from datetime import datetime

from scrollphathd.fonts import font5x7, font5x5

from argparse import ArgumentParser

try:
    from queue import Queue, Empty
except ImportError:
    from Queue import Queue, Empty

from .action import Action
from .stoppablethread import StoppableThread

try:
    import http.client as http_status
except ImportError:
    import httplib as http_status

from flask import Blueprint, render_template, abort, request, jsonify, Flask

scrollphathd_blueprint = Blueprint('scrollhat', __name__)
api_queue = Queue()


class AutoScroll():
    _is_enabled = False
    _interval = 0.1

    def config(self, is_enabled="False", interval=0.1):
        self._interval = interval

        if is_enabled == "True":
            if self._is_enabled is False:
                self._is_enabled = True
                self.run()
        else:
            self._is_enabled = False

    def run(self):
        if self._is_enabled is True:
            # Start a timer
            threading.Timer(self._interval, self.run).start()
            # Scroll the buffer content
            scrollphathd.scroll()
            # Show the buffer
            scrollphathd.show()


@scrollphathd_blueprint.route('/autoscroll', methods=["POST"])
def autoscroll():
    response = {"result": "success"}
    status_code = http_status.OK
    try:
        api_queue.put(Action("autoscroll", (data["is_enabled"], float(data["interval"]))))
    except KeyError:
        response = {"result": "KeyError", "error": "keys is_enabled and interval not posted."}
        status_code = http_status.UNPROCESSABLE_ENTITY
    except ValueError:
        response = {"result": "ValueError", "error": "invalid data type(s)."}
        status_code = http_status.UNPROCESSABLE_ENTITY

    return jsonify(response), status_code


@scrollphathd_blueprint.route('/start', methods=["POST"])
def start():
    response = {"result": "success"}
    status_code = http_status.OK

    try:
        api_queue.put(Action("start_timer", {}))
    except ValueError:
        response = {"result": "ValueError", "error": "invalid integer."}
        status_code = http_status.UNPROCESSABLE_ENTITY

    return jsonify(response), status_code


@scrollphathd_blueprint.route('/stop', methods=["POST"])
def stop():
    response = {"result": "success"}
    status_code = http_status.OK
    try:
        api_queue.put(Action("stop_timer", {}))
    except ValueError:
        response = {"result": "ValueError", "error": "invalid integer."}
        status_code = http_status.UNPROCESSABLE_ENTITY

    return jsonify(response), status_code


@scrollphathd_blueprint.route('/pause', methods=["POST"])
def pause():
    response = {"result": "success"}
    status_code = http_status.OK
    try:
        api_queue.put(Action("pause", {}))
    except ValueError:
        response = {"result": "ValueError", "error": "invalid integer."}
        status_code = http_status.UNPROCESSABLE_ENTITY

    return jsonify(response), status_code


@scrollphathd_blueprint.route('/resume', methods=["POST"])
def resume():
    response = {"result": "success"}
    status_code = http_status.OK
    try:
        api_queue.put(Action("resume", {}))
    except ValueError:
        response = {"result": "ValueError", "error": "invalid integer."}
        status_code = http_status.UNPROCESSABLE_ENTITY

    return jsonify(response), status_code


@scrollphathd_blueprint.route('/start-long-break', methods=["POST"])
def start_long_break():
    response = {"result": "success"}
    status_code = http_status.OK
    try:
        api_queue.put(Action("start_long_break", {}))
    except ValueError:
        response = {"result": "ValueError", "error": "invalid integer."}
        status_code = http_status.UNPROCESSABLE_ENTITY

    return jsonify(response), status_code


@scrollphathd_blueprint.route('/stop-long-break', methods=["POST"])
def stop_long_break():
    response = {"result": "success"}
    status_code = http_status.OK
    try:
        api_queue.put(Action("stop_long_break", {}))
    except ValueError:
        response = {"result": "ValueError", "error": "invalid integer."}
        status_code = http_status.UNPROCESSABLE_ENTITY

    return jsonify(response), status_code

@scrollphathd_blueprint.route('/start-break', methods=["POST"])
def start_break():
    response = {"result": "success"}
    status_code = http_status.OK
    try:
        api_queue.put(Action("start_break", {}))
    except ValueError:
        response = {"result": "ValueError", "error": "invalid integer."}
        status_code = http_status.UNPROCESSABLE_ENTITY

    return jsonify(response), status_code


@scrollphathd_blueprint.route('/stop-break', methods=["POST"])
def stop_break():
    response = {"result": "success"}
    status_code = http_status.OK
    try:
        api_queue.put(Action("stop_break", {}))
    except ValueError:
        response = {"result": "ValueError", "error": "invalid integer."}
        status_code = http_status.UNPROCESSABLE_ENTITY

    return jsonify(response), status_code


@scrollphathd_blueprint.route('/rotate', methods=["POST"])
def rotate():
    response = {"result": "success"}
    status_code = http_status.OK
    try:
        api_queue.put(Action("rotate", {}))
    except ValueError:
        response = {"result": "ValueError", "error": "invalid integer."}
        status_code = http_status.UNPROCESSABLE_ENTITY

    return jsonify(response), status_code


@scrollphathd_blueprint.route('/clear', methods=["POST"])
def clear():
    response = {"result": "success"}
    status_code = http_status.OK

    api_queue.put(Action("clear", {}))
    return jsonify(response), status_code

@scrollphathd_blueprint.route('/new-timer', methods=["POST"])
def new_timer():
    response = {"result": "success"}
    status_code = http_status.OK

    data = request.get_json()
    if data is None:
        data = request.form
    try:
        duration_str = data["start_duration"]
        formats = ["%M:%S", "%-M:%S"]

        parsed_duration = None

        for format_str in formats:
            try:
                parsed_duration = datetime.strptime(duration_str, format_str)
                break
            except ValueError:
                pass

        print(parsed_duration.minute)  # Prints the minutes part (5)
        print(parsed_duration.second)  # Prints the seconds part (30)
        api_queue.put(Action("new_timer", data))
    except KeyError:
        response = {"result": "KeyError", "error": "key start_duration is not posted."}
        status_code = http_status.UNPROCESSABLE_ENTITY
    except ValueError:
        response = {"result": "ValueError", "error": "invalid data type(s)."}
        status_code = http_status.UNPROCESSABLE_ENTITY

    return jsonify(response), status_code




def cleanup():
    # Reset the autoscroll
    autoscroll.config()
    # Clear the buffer before writing new text
    scrollphathd.clear()
    
# Brightness of the seconds bar and text
BRIGHTNESS = 0.3

# Timer start delay to account for Pomdone start timers in secconds
START_DELAY = 0

# MAin task timer duration
FULL_TASK_DURATION = 1 + 25 * 60 - START_DELAY
EXACT_TASK_DURATION = 25 * 60
BREAK_DURATION = 1 + 5 * 60 - START_DELAY
LONG_BREAK_DURATION = 1 + 15 * 60 - START_DELAY

# Display a progress bar for seconds
# Displays a dot if False
DISPLAY_BAR = True

def run():

    start = 0
    elapsed_time = 0
    countdown_running = False
    finish_countdown = False
    current_timer_duration = FULL_TASK_DURATION
    exact_timer_duration = EXACT_TASK_DURATION
    display_orientation = 180
    scrollphathd.rotate(degrees=display_orientation)

    while True:
        # Check if there's any HTTP actions
        if (api_queue.qsize() > 0):
            action = api_queue.get(block=True)

            if action.action_type == "start_timer":
                cleanup()
                
                current_timer_duration = FULL_TASK_DURATION
                start = time.time()
                elapsed_time = 0
                countdown_running = True
                finish_countdown = False

            if action.action_type == "stop_timer":
                current_timer_duration = FULL_TASK_DURATION
                start = 0
                elapsed_time = 0
                countdown_running = True
                finish_countdown = True

            if action.action_type == "start_break":
                cleanup()

                current_timer_duration = BREAK_DURATION
                start = time.time()
                elapsed_time = 0
                countdown_running = True
                finish_countdown = False

            if action.action_type == "stop_break":
                current_timer_duration = BREAK_DURATION
                start = 0
                elapsed_time = 0
                countdown_running = True
                finish_countdown = True

            if action.action_type == "start_long_break":
                cleanup()

                current_timer_duration = LONG_BREAK_DURATION
                start = time.time()
                elapsed_time = 0
                countdown_running = True
                finish_countdown = False

            if action.action_type == "stop_long_break":
                current_timer_duration = LONG_BREAK_DURATION
                start = 0
                elapsed_time = 0
                countdown_running = True
                finish_countdown = True

            if action.action_type == "pause" and countdown_running and not finish_countdown:
                elapsed_time = elapsed_time + time.time() - start
                countdown_running = False
                finish_countdown = False

            if action.action_type == "resume" and not countdown_running and not finish_countdown and start > 0:
                start = time.time()
                countdown_running = True
                finish_countdown = False

            if action.action_type == "rotate":
                if (display_orientation == 180):
                    display_orientation = 0
                else:
                    display_orientation = 180
                scrollphathd.rotate(degrees=display_orientation)
            if action.action_type == "clear":
                cleanup()
                countdown_running = False
                finish_countdown = False
            if action.action_type == "new_timer":
                cleanup()

                duration_str = action.data["start_duration"]
                formats = ["%M:%S", "%-M:%S"]

                start_duration = None

                for format_str in formats:
                    try:
                        start_duration = datetime.strptime(duration_str, format_str)
                        break
                    except ValueError:
                        pass
                current_timer_duration = start_duration.minute * 60 + start_duration.second
                start = time.time()
                elapsed_time = 0
                countdown_running = not action.data.get("is_paused", False)
                finish_countdown = False


        if countdown_running:
            now=time.time()
            now_elapsed_time = now - start + elapsed_time
            time_remaining = current_timer_duration - now_elapsed_time
            if time_remaining>0 and not finish_countdown:
                secs=int(time_remaining)
                
                text=datetime.fromtimestamp(secs).strftime("%M:%S")
                scrollphathd.clear()

                # Grab the "seconds" component of the current time
                # and convert it to a range from 0.0 to 1.0
                float_sec = (time_remaining) / current_timer_duration * 1.0
            
                # Multiply our range by 15 to spread the current
                # number of seconds over 15 pixels.
                #
                # 60 is evenly divisible by 15, so that
                # each fully lit pixel represents 4 seconds.
                #
                # For example this is 28 seconds:
                # [x][x][x][x][x][x][x][ ][ ][ ][ ][ ][ ][ ][ ]
                #  ^ - 0 seconds                59 seconds - ^
                seconds_progress = float_sec * 15
            
                if DISPLAY_BAR:
                    # Step through 15 pixels to draw the seconds bar
                    for y in range(15):
                        # For each pixel, we figure out its brightness by
                        # seeing how much of "seconds_progress" is left to draw
                        # If it's greater than 1 (full brightness) then we just display 1.
                        current_pixel = min((15 - seconds_progress), 1)
            
                        # Multiply the pixel brightness (0.0 to 1.0) by our global brightness value
                        scrollphathd.set_pixel(y + 1, 6, current_pixel * BRIGHTNESS)
            
                        # Subtract 1 now we've drawn that pixel
                        seconds_progress -= 1
            
                        # If we reach or pass 0, there are no more pixels left to draw
                        if seconds_progress <= 0:
                            break
            
                else:
                    # Just display a simple dot
                    scrollphathd.set_pixel(int(seconds_progress), 6, BRIGHTNESS)
                

                # Display the time (HH:MM) in a 5x5 pixel font
                scrollphathd.write_string(
                    text,
                    x=0, # Align to the left of the buffer
                    y=0, # Align to the top of the buffer
                    font=font5x5, # Use the font5x5 font we imported above
                    brightness=BRIGHTNESS # Use our global brightness value
                )

            
                # int(time.time()) % 2 will tick between 0 and 1 every second.
                # We can use this fact to clear the ":" and cause it to blink on/off
                # every other second, like a digital clock.
                # To do this we clear a rectangle 8 pixels along, 0 down,
                # that's 1 pixel wide and 5 pixels tall.
                if int(time.time()) % 2 == 0:
                    scrollphathd.clear_rect(8, 0, 1, 5)
            
            else:
                print("Finished countdown")
                countdown_running = False
                finish_countdown = True
                scrollphathd.clear()
                scrollphathd.write_string(
                    "Time's up! ",
                    x=0, # Align to the left of the buffer
                    y=0, # Align to the top of the buffer
                    font=font5x7, # Use the font5x5 font we imported above
                    brightness=BRIGHTNESS # Use our global brightness value
                )
           
        if finish_countdown:
            scrollphathd.scroll()

        # Display our time and sleep a bit. Using 1 second in time.sleep
        # is not recommended, since you might get quite far out of phase
        # with the passing of real wall-time seconds and it'll look weird!
        #
        # 1/10th of a second is accurate enough for a simple clock though :D
        scrollphathd.show()
        time.sleep(0.02)


def start_background_thread():
    api_thread = StoppableThread(target=run)
    api_thread.start()


scrollphathd_blueprint.before_app_first_request(start_background_thread)


# Autoscroll handling
autoscroll = AutoScroll()


def main():
    # Parser handling
    parser = ArgumentParser()
    parser.add_argument("-p", "--port", type=int, help="HTTP port.", default=8080)
    parser.add_argument("-H", "--host", type=str, help="HTTP host.", default="0.0.0.0")
    args = parser.parse_args()

    # TODO Check
    #scrollphathd.rotate(degrees=180)
    scrollphathd.set_clear_on_exit(True)
    scrollphathd.write_string(str(args.port), x=0, y=0, brightness=0.1)
    scrollphathd.show()

    # Flash usage
    app = Flask(__name__)
    app.register_blueprint(scrollphathd_blueprint, url_prefix="/scrollphathd")
    app.run(port=args.port, host=args.host)


if __name__ == '__main__':
    main()

